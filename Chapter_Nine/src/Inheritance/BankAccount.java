package Inheritance;

/**
 * Created by azzumwaqar on 25/05/2017.
 */
public class BankAccount {

    private double balance;

    /**
     * Gets the current balance of this account*/
    public double getBalance() {
        return balance;
    }

    /**
     * Makes a deposit into this account
     * */
    public void deposit(double amount){
        balance+=amount;
    }

    /**
     * Makes a withdrawal from this account
     * Charges a penalty if sufficient funds are not available
     * @param amount the amount to be withdrawn
     * */
    public void withdraw(double amount){
        balance -= amount;
    }

    /**
     * Carries out month end processing that is appropriate for this account
     * */
    public void monthEnd(){

    }

}
