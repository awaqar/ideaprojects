package Inheritance;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by azzumwaqar on 25/05/2017.
 */
public class Main {


    public static void main(String [] args){

         BankAccount[] accounts = new BankAccount[2];
       // ArrayList<BankAccount> accounts = new ArrayList<BankAccount>();

        BankAccount saving = new Savings();
        BankAccount checking = new Checking();
        accounts[0] = saving;
        accounts[1] = checking;

        Scanner in = new Scanner(System.in);
        boolean done = false;


        while(!done){
            System.out.print("D)eposit\tW)ithdraw\tM)onth end\tQ)uit");
            String input = in.next();

            if(input.equals("D")||input.equals("W")){
                System.out.print("Enter account number and amount: ");
                int acc_num = in.nextInt();  //0/1
                double amount = in.nextDouble();

                if(input.equals("D")){
                    accounts[acc_num].deposit(amount);
                }
                else
                    accounts[acc_num].withdraw(amount);

                System.out.println("Balance: "+ accounts[acc_num].getBalance());
            }

            else if(input.equals("M")){
                for(int n=0; n<accounts.length; n++){
                    accounts[n].monthEnd();
                    System.out.println(n + accounts[n].getBalance());
                }
            }

            else if(input.equals("Q")){
                //!true would be false which turns while(!done) to false;
                done=true;
            }
        }


    }



}
