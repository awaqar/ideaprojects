package Inheritance;

/**
 * Created by azzumwaqar on 25/05/2017.
 */
public class Savings extends BankAccount{

    private double interestRate;
    private double minBalance;
    /**
     * Constructor with zero balance initialised
     */

    public Savings(){

    }

    /**
     * Sets interest rate for this account
     * */
    public void setInterestRate(){

    }

    @Override
    public void withdraw(double amount) {
        super.withdraw(amount);
        double balance = getBalance();
        if(balance < minBalance){
            minBalance = balance;
        }

    }

    @Override
    public void monthEnd() {
       double interest = minBalance * interestRate/100;

       //add/deposit this amount into the account
       deposit(interest);

       //reset minBalance
        minBalance = getBalance();
    }
}
