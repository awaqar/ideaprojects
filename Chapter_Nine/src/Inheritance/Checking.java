package Inheritance;

/**
 * Created by azzumwaqar on 25/05/2017.
 */
public class Checking extends BankAccount{
    private int transactions;

    private final int MAX_TRANSACTIONS = 3;
    private final double WITHDRAWAL_FEE = 1.0;

    /**
     * Constructor checking account with zero balance
     * */
    public Checking(){}


/*    public int getWithdrawls() {
        return withdrawls;
    }
*/
    //public int getDeposits(){return deposits;}

    @Override
    public void withdraw(double amount) {
        super.withdraw(amount);
        transactions++;
        levyFees();
    }

    @Override
    public void monthEnd() {
        transactions = 0;
    }

    @Override
    public void deposit(double amount) {
        super.deposit(amount);
        transactions++;
        levyFees();
    }

    private void levyFees(){
        if(transactions>MAX_TRANSACTIONS){
            super.withdraw(WITHDRAWAL_FEE);
        }
    }
}
