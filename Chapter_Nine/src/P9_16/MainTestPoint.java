package P9_16;

import java.awt.*;

/**
 * Created by azzumwaqar on 30/05/2017.
 * P9.17: Reimplement LabeledPoint class of P9.16 by storing the location
 * in ajava.awt. Point object. Your toString() method should invoke the toString method
 * of the Point class.
 */
public class MainTestPoint {
    public static void main(String [] args){
        Point point = new LabeledPoint(4,5,"Corner");

        System.out.print(point.toString());
    }
}
