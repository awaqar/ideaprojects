package P9_16;

import java.awt.*;

/**
 * Created by azzumwaqar on 30/05/2017.
 */
public class LabeledPoint extends Point{
    //private int x;
    //private int y;
    private String label;

    public LabeledPoint(int x, int y, String label){
        //this.x = x;
        //this.y = y;
        super(x,y);
        this.label = label;
    }

    /*public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }*/

    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        //return getClass().getSimpleName() + "\nX: "+ getX() + "\nY: " + getY() + "\nLabel: " + getLabel();
        return super.toString();
    }
}
