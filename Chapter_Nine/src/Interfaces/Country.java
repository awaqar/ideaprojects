package Interfaces;

/**
 * Created by azzumwaqar on 27/04/2017.
 */
public class Country implements Measureable,Comparable<Country> {

    private double area;
    private String name;

    public Country(String name, double area){
        this.area = area;
        this.name = name;
    }

    @Override
    public double getMeasure() {
        return area;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Country country) {

        if(this.area < country.getMeasure()){return -1;}
        if(this.area > country.getMeasure()){return 1;}

        return 0;
    }


}
