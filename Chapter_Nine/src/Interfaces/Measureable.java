package Interfaces;

import java.util.Collection;

/**
 * Created by azzumwaqar on 27/04/2017.
 */
public interface Measureable{

    // cannot have instance variables
    //CAN HAVE constants
    // all variables are public static final
    // so a constant must be written without public static final

    /*methods are always public and abstract*/
    double getMeasure();
    String getName();

}
