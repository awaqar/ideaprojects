package Interfaces;

import java.util.Arrays;

/**
 * Created by azzumwaqar on 27/04/2017.
 */
public class MeasurableDemo {

    public static void main (String [] args){

        Measureable [] accounts = new BankAccount[3];

        accounts[0] = new BankAccount(0);
        accounts[1] = new BankAccount(10000);
        accounts[2] = new BankAccount(2000);

        System.out.println("Average Balance: " + average(accounts));

        Measureable [] countries = new Country[5];
        countries[0] = new Country("Pakistan",12000);
        countries[1] = new Country("UK",8000);
        countries[2] = new Country("Islamic State",30000);
        countries[3] = new Country("Scotland",5000);
        countries[4] = new Country("Russia",22000);


        System.out.println("Average Area: " + average(countries));

        Measureable[] people = new Measureable[3];
        people[0] = new Person("Azzum",5.8);
        people[1] = new Person("Anam",5.3);
        people[2] = new Person("Emre",6.0);

        System.out.println("Avergae height: " + average(people));

        System.out.println("Largest country: " + maximum(countries).getName());

        System.out.println("Largest country area: " + max(countries).getName());

        System.out.println("Tallest Person: "+ max(people).getName());
        System.out.println("Tallest Person using maximum: "+ maximum(people).getName());
    }

    public static double average(Measureable []objs){
        if(objs.length ==0) return 0;

        double sum =0;

        for(Measureable obj:objs){
            sum += obj.getMeasure();
        }

        return sum/objs.length;
    }


    /**@return Maximum of maximum of the measurable array
     * @param objs
     * takes an array of type Measurable and returns the maximum of those
     * */
    public static Measureable maximum(Measureable [] objs){
        double largest = objs[0].getMeasure();
        Measureable m = objs[0];

        for(int i=1; i<objs.length; i++){

            //if obj in index[] > first element
            if(objs[i].getMeasure() > largest){
                //update the largest variable
                largest = objs[i].getMeasure();

                //update the Measurable obj
                m = objs[i];

            }
        }

        return m;

    }

    /**
     * sorts the Measurable array
     * and extracts the last element from the array
     * */
    public static Measureable max(Measureable [] objs) {
        Measureable m;
        Arrays.sort(objs);
        m = objs[objs.length-1];

        return m;
    }

}
