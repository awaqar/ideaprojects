package Interfaces;

/**
 * Created by azzumwaqar on 27/04/2017.
 */

/*A class can implement more than one interface e.g. LinkedList implements List and Que*/
public class BankAccount implements Measureable,Comparable<BankAccount> {

    private double balance;
    public BankAccount(double pbalance){
        balance = pbalance;
    }

    public double getMeasure(){
        return balance;
    }

    @Override
    public String getName() {
        return null;
    }

    @Override
    public int compareTo(BankAccount other) {
        if(this.balance < other.getMeasure()){return -1;}
        if(this.balance > other.getMeasure()){return 1;}
        return 0;
    }
}
