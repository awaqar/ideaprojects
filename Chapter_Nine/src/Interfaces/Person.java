package Interfaces;

/**
 * Created by azzumwaqar on 11/06/2017.
 */
public class Person implements Measureable,Comparable<Person> {
   private double height;
   private String name;

   public Person(String name, double height){
       this.name = name;
       this.height = height;
   }
    @Override
    public double getMeasure() {
        return height;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int compareTo(Person person) {
        if(this.height < person.getMeasure()){return -1;}
        if(this.height > person.getMeasure()){return 1;}
       return 0;
    }
}
