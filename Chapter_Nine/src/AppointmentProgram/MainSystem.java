package AppointmentProgram;

import org.omg.CORBA.DATA_CONVERSION;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by azzumwaqar on 25/05/2017.
 */
public class MainSystem {

    public static void main(String [] args){

        /*
            Array List of Appointments
        * */
        ArrayList<Appointment> appointments = new ArrayList<Appointment>();

        /*
        * Initial fill of the appointments arrayList
        * */
        appointments.add(new Daily(2017,3, 22,"see the dentist","13:40"));
        appointments.add(new Monthly(2017,2,5,"see the psychiatrist","14:00"));
        appointments.add(new Weekly(2017,4,3,"see optician","09:30"));
        appointments.add(new OneTime(2017,8,5,"blood test","09:00"));



        /*
            An Array of options to display to the user
            so a user can choose between searching for appointments or booking and appointment
        * */
        String [] options = {"Search Appointments","Book an Appointment"};

        /*
            Another array list to display to user the type of appointments to choose from
        */
        ArrayList<String> appointment_options = new ArrayList<String>();
        appointment_options.add("One Time");
        appointment_options.add("Daily");
        appointment_options.add("Weekly");
        appointment_options.add("Monthly");

        //variables
        boolean done = false;
        int input=-1;

        /*Keep showing the option menu until user enters the correct input: an integer between 0 and 1.
        * Catch exception if user enters a non-integer value.
        * */
        do{
            try{

                //display menu: search appointment or book an appointment
                Scanner in  = new Scanner(System.in);
                System.out.println("0) "+options[0]);
                System.out.println("1) "+options[1]);
                System.out.print("Enter the option number: ");
                input= in.nextInt();
                done = true;

            }catch(InputMismatchException ie){
                System.out.println("Please enter a number between 0 and 1");
            }


        }while(!done || (input<0 ||input>1));


        Scanner in = new Scanner(System.in);

        //search appointment
        if(input==0){


            System.out.print("Enter year: ");
            int year = in.nextInt();

            System.out.print("Enter month: ");
            int month = in.nextInt();

            System.out.print("Enter day: ");
            int day = in.nextInt();

            int count =0;
            //if an appointment occurson that day print it out.
            for(int i=0; i <appointments.size();i++){

                if(appointments.get(i).occursOn(year,month,day)) {
                    count++;
                    System.out.println("Appointment " +count+ ") "+"\nDesciption: "+appointments.get(i).getDescription() + " at "+ appointments.get(i).getTime());
                    System.out.println("Date: " + appointments.get(i).getDate());
                    System.out.println();
                }

                // else System.out.println("No appointmnets");
            }

        }

        /*Exercise: P9.9 - allow user to book an appointment*/
        else{

            //ask for the date
            int year,month,day,option,hours,mins;
            String desc,time, sHours, sMins;

            /*
            * Keep asking for year until user enters 2017
            * */
            do{
                System.out.print("Enter year: ");
                year = in.nextInt();
                if(year!=2017) System.out.println("Can only book 2017 appointments");
            }while(year!=2017);

            /*
            * Keep asking for month number until user enters
            * month number between 1 and 12
            * */
            do{
                System.out.print("Enter month: ");
                month = in.nextInt();
                if (month<1||month>12) System.out.println("Enter month between 1 and 12");
            }while(month<1||month>12);

            /*
            * Keep asking user for the day of the month
            * until user enters a day between 1 and 30
            * */
            do{
                System.out.print("Enter day: ");
                day = in.nextInt();
                if(day<1||day>30) System.out.println("Enter day number between 1 and 30");
            }while(day<1||day>30);

            /*
            * Ask user for the Time - stored as string
            * Ask for the hours and mins: hours between 0 and 23
             * mins between 0 and 59*/
            do{
                System.out.println("Enter time, e.g (13:30): ");
                System.out.print("Enter hours 00-23: ");
                hours = in.nextInt();
                System.out.print("Enter mins: ");
                mins = in.nextInt();
            }while((hours<0 || hours>23) || (mins <0 || mins>59));


            /*
            * Change the hours and mins to one single string Time
            * if user enters digits 0 to 9 it prepends a spare 0
            * */
            if(hours==0||hours<=9){
                sHours = "0"+hours;
            }
            else{
                sHours = ""+hours;
            }

            if(mins==0||mins<=9){
                sMins = "0"+mins;
            }
            else {
                sMins = ""+mins;
            }


            time = ""+sHours + ":"+sMins;

            /*
            * Ask for a short description about the appointment e.g. to see Dr.
            * keeps asking user for it until user enters the string
            * */
            Scanner sInput = new Scanner(System.in);
            do{

                System.out.print("Enter a short description e.g. see dentist: ");
                desc = sInput.nextLine();
            }while(!sInput.hasNextLine());


            /*
             * ask for the type of appointment
             * list all the appointment types
             * keeps displaying them until user
             * enters the correct input between 0 and options size*/
            do{
                System.out.println("Specify the appointment type:");
                //show the options
                for(int i=0;i<appointment_options.size();i++){
                    System.out.println(i+") "+appointment_options.get(i));
                }
                System.out.print("Enter option number: ");
                option = in.nextInt();
            }while(option<0||option>=appointment_options.size());

           /*
           * depending on the option selected create the corresponding appointment object
           * */

            switch (option){
                case 0: appointments.add(new OneTime(year,month,day,desc,time));
                    System.out.println("Your One Time appointment has been booked");break;
                case 1: appointments.add(new Daily(year,month,day,desc,time));
                    System.out.println("Your Daily appointment has been booked");break;
                case 2: appointments.add(new Weekly(year,month,day,desc,time));
                    System.out.println("Your Weekly appointment has been booked");break;
                case 3: appointments.add(new Monthly(year,month,day,desc,time));
                    System.out.println("Your Monthly appointment has been booked");break;


            }

            //confirm and print out appointment details
            System.out.print("Your "+ appointment_options.get(option) + " Appointment: " + appointments.get(appointments.size()-1).getDescription() +" is booked for ");
            System.out.print(appointments.get(appointments.size()-1).getTime() + " on ");
            System.out.print(appointments.get(appointments.size()-1).getDate());


            String str_save;

            do {
                System.out.print("\nWould you like to save this appointment?");
                System.out.print("Enter Y for Yes, N for No: ");
                str_save = sInput.next();
            }while(str_save.matches("[^(Y|N)]"));

            if(str_save.equals("Y")){

                //save the file
                try{
                    save(appointments.get(appointments.size()-1),option);
                }catch(FileNotFoundException fe){
                    System.out.println("File could not be found");
                }

                System.out.println("Your appointment has been saved.");
            }
            else
                System.exit(0);

        }


    }

    /**
     * P9.10
     * @param ap - appointment type
     * @param  option - int option number selected
     * @throws FileNotFoundException
     * This method saves the details of the appointment in a text file named
     * according to the option and appointment type. Details are extracted
     * from the toString method of the Appointment Class and written to the
     * file using the PrintWriter object
     * */
    public static void save(Appointment ap,int option) throws FileNotFoundException {
        //boolean filesaved = false;
        String filename="";

        switch (option){
            case 0: filename = "OneTime";break;
            case 1: filename = "Daily";break;
            case 2: filename = "Weekly";break;
            case 3: filename = "Monthly";break;
        }
        filename+=".txt";

        //this syntax obviates finally clause
        //automatically calls the close() on PrintWriter/Scanner object
        try(PrintWriter out = new PrintWriter(filename)){
            out.println(ap.toString());

        }
        catch (FileNotFoundException fe){
            System.out.println("File could not be saved.");
        }
    }


}
