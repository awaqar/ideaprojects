package AppointmentProgram;

import java.util.Date;

/**
 * Created by azzumwaqar on 25/05/2017.
 * Implement a superclass Appointment and subclass: OneTime, Daily, Weekly and Monthly;
 * An appointment has a description (e.g.see the dentist) and a date and a time;
 * Write a method occursOn(int year, int month, int day) that checks whether the appointment
 * occurs on that date. For example, for a monthly appointment, you must check whether the day of
 * the month matches;
 * Then, fill an array of Appointment objects with a mixture of appointments.
 * Have the user enter a date and print out all appointments that occur on that date.
 */
public  class Appointment {

    private String description;
    private int month;
    private int day;
    private int year;
    private String time;


    public Appointment(int year, int month, int day, String description, String time){
        this.year = year;
        this.day = day;
        this.month = month;
        this.description = description;
        this.time = time;
    }


    public  boolean occursOn(int year, int month, int day){

        boolean occurs = false;
        if(year< getYear()){
            System.out.println("previous years not allowed!");
        }
        else if(this.year ==year){
            if(this.month ==month){
                if(this.day == day) occurs= true;

                else return occurs;
            }
            else return occurs;
        }

        return occurs;

    }

    public String getDescription() {
        return description;
    }

    public int getMonth() {
        return month;
    }

    public int getDay() {
        return day;
    }

    public int getYear() {
        return year;
    }

    public String getTime() {
        return time;
    }

    public String getDate(){
        return ""+getDay()+"/"+getMonth()+"/"+getYear();
    }

    @Override
    public String toString() {
        //return super.toString();
        String info="";
        info+=getClass().getSimpleName()+" Appointment";
        info+="\nDescription: "+ getDescription();
        info+= "\nTime: "+ getTime();
        info+="\nDate: "+ getDate();

        return info;
    }
}
