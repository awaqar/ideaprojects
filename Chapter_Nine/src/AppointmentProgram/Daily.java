package AppointmentProgram;

import java.util.Date;

/**
 * Created by azzumwaqar on 25/05/2017.
 */
public class Daily extends Appointment {
    public Daily(int year, int month, int day, String description,String time){
        super(year,month,day,description,time);
    }

    @Override
    public boolean occursOn(int year, int month, int day) {
        boolean occurs = false;
        if(year>=this.getYear()){
            occurs = true;
        }

        return occurs;
    }


}
