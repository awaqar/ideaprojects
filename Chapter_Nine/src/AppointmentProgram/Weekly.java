package AppointmentProgram;

/**
 * Created by azzumwaqar on 25/05/2017.
 */
public class Weekly extends Appointment {

    public Weekly(int year, int month, int day, String description,String time){
        super(year,month,day,description,time);
    }

    //day 3: month 4 : 2017
    @Override
    public boolean occursOn(int year, int month, int day) {
        //month = 4
        //day = 10
        //year = 2017

        boolean occurs = false;

        if(year < getYear()) {
            System.out.print("Appointments cannot be shown for previous years");
            return occurs;
        }

        if(year == getYear()){
            if (month<getMonth()){
                System.out.println("Previous dates not allowed");
                occurs = false;
            }


            if(month>=getMonth()){
                for(int i=getDay(); i<=31; i=i+7){

                      if(i>30){
                        i=1;
                        //System.out.println("else if > 30: "+i);
                        }

                     else if(i>=24&&i<=30){
                        int itemp = (30-i); //6 , 7-6=1
                        int date=7-itemp;
                        i=date;
                    }

                     if(i == day) {
                        occurs = true;
                        //System.out.println("i: "+i + " Date: "+date + " Day "+ day);
                        break;
                    }

                }

            }
        }

        return occurs;

    }

}
