package AppointmentProgram;

/**
 * Created by azzumwaqar on 25/05/2017.
 */
public class Monthly extends Appointment{

    public Monthly(int year, int month, int day, String description,String time){
        super(year,month,day,description,time);
    }

    @Override
    public boolean occursOn(int year, int month, int day) {
        //return super.occursOn(year, month, day);
        boolean occurs = false;
        if(year == getYear()) {
            if (this.getDay() == day) {
                occurs = true;
            }
        }
        return occurs;
    }
}
