package AppointmentProgram;

/**
 * Created by azzumwaqar on 25/05/2017.
 */
public class OneTime  extends Appointment{

    public OneTime(int year, int month, int day, String description,String time){
        super(year,month,day,description,time);
    }
}
