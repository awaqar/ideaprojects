package P9_12;

/**
 * Created by azzumwaqar on 30/05/2017.
 */
public class Student extends Person {

    private String major;

    public Student(String name, int yob, String major){
        super(name,yob);
        this.major = major;
    }

    public String getMajor() {
        return major;
    }

    @Override
    public String toString() {
        return super.toString() + " Major: "+ getMajor();
    }
}
