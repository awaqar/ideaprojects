package P9_12;

/**
 * Created by azzumwaqar on 30/05/2017.
 */
public class MainTestClass {
    public static void main(String [] args){
        Person person = new Person();

        Student student = new Student("Azzum",260190,"Computer Science");
        Instructor instructor = new Instructor("Danish",230475, 45000);

        System.out.println(student.toString());
        System.out.println(instructor.toString());
    }
}
