package P9_12;

/**
 * Created by azzumwaqar on 30/05/2017.
 */
public class Person {
    private String name;
    private int yob;

    public Person(){}

    public Person(String name, int year_of_birth){
        this.name = name;
        this.yob = year_of_birth;
    }
    public String getName() {
        return name;
    }

    public int getYob() {
        return yob;
    }

    @Override
    public String toString() {
       // return super.toString();
        return getClass().getSimpleName() + " Name: "+ getName() + " Year of Birth: "+ getYob();
    }
}
