package P9_12;

/**
 * Created by azzumwaqar on 30/05/2017.
 */
public class Instructor extends Person{
    private double salary;

    public Instructor(String name,int year,double salary){
        super(name,year);
        this.salary = salary;
    }

    public double getSalary() {
        return salary;
    }

    @Override
    public String toString() {
        return super.toString() + " Salary: "+ getSalary();
    }
}
