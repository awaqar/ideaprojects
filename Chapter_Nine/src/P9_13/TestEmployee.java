package P9_13;

/**
 * Created by azzumwaqar on 30/05/2017.
 */
public class TestEmployee {
    public static void main(String [] args){
        Executive executive = new Executive("John Dale",23000,"Sales");
        Manager manager = new Manager("barnard",30000,"Sales Blah");

        System.out.println(executive.toString());
        System.out.println(manager.toString());

    }
}
