package P9_13;

/**
 * Created by azzumwaqar on 30/05/2017.
 */
public class Manager extends Employee {
    private String department;
    public Manager(String name,double salary, String depart){
        super(name,salary);
        this.department = depart;
    }

    public String getDepartment() {
        return department;
    }

    @Override
    public String toString() {
        return super.toString() + "\nDepartment: "+ getDepartment();
    }
}
