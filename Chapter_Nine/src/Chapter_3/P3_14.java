package Chapter_3;

import java.util.Scanner;

/**
 * Created by azzumwaqar on 26/05/2017.
 * This program asks for intitial balances of bank account; reject negative balances.
 * Then asks for transactions; options are Deposit, Withdraw and Transfer.
 * Then ask for the account: checking || savings.
 * Then ask for the amount; reject transactions that overdraw an account.
 * At the end print balances of both accounts.
 */
public class P3_14 {

    public static void main(String [] args) {
       double chk_balance=0;
       double saving_balance =0;
       Scanner in ;
       String [] options = {"Deposit","Withdraw","Transfer"};

        //Ask user for the initial balances of accounts
        do {

                in = new Scanner((System.in));
                System.out.print("Enter balance for Checking Account: ");
                chk_balance = in.nextDouble();
                System.out.print("Enter balance for Savings Account: ");
                saving_balance = in.nextDouble();

        }while(chk_balance<0 || saving_balance<0);

        //print transaction options
        for(int i=0; i<options.length; i++){

            System.out.println(i+") "+ options[i]);
        }

        //ask for transactions now
        int option = 0;

        do{
            System.out.print("Enter option number: ");
             option = in.nextInt();
        }while(option>=options.length || option<0);

        //ask for account type
        String account_type;
        do {

            System.out.print("Checking or Savings - Enter C for Checking, S for Savings: ");
             account_type = in.next();
        }while(account_type.matches("[^(S|C)]"));


        //ask for the amount; reject the amount if it overdraws the balance
        //Switch statement to append the word commensurate with the option chosen
        double amount;
        String selected_option = "";
        switch (option){
            case 0: selected_option = "Deposit";break;
            case 1: selected_option = "Withdrawn";break;
            case 2: selected_option = "Transfered";break;
        }

        //account selection
        double account_selected = account_type.equals("S")? saving_balance:chk_balance;
        double account_not_selected = account_type.equals("S")? chk_balance:saving_balance;

        //reject transactions if the amount is greater than the balance
        do{
            System.out.print("Enter amount to be "+ selected_option + ": ");
            amount = in.nextDouble();
            if(amount>account_selected)  System.out.print("insufficient funds: current balance: £"+account_selected+ "\n");
        }while(amount > account_selected);


        switch (option){
            //incase of deposit
            case 0: account_selected =account_selected + amount;break;

            //incase of withdraw
            case 1: account_selected = account_selected - amount; break;

            //incase of transfer; we would withdraw amount; then deposit into another account
            case 2:

                    account_selected = account_selected - amount;
                    account_not_selected += amount;break;
        }

        //choosing right account name according to the balance.
        String acc_name ="";
        String acc_name_other= "";

        if(account_type.equals("S")){
            acc_name = "Saving Account: ";
            acc_name_other = "Checking Account: ";
        }

        else{
            acc_name = "Checking Account: ";
            acc_name_other = "Saving Account: ";
        }

        System.out.println("Balance for " + acc_name + account_selected );
        System.out.println("Balance for "+ acc_name_other + account_not_selected);
    }
}
