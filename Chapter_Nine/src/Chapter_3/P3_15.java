package Chapter_3;

import java.util.Scanner;

/**
 * Created by azzumwaqar on 26/05/2017.
 */
public class P3_15 {
    public static void main(String [] args){

        Scanner in = new Scanner((System.in));
        System.out.print("Enter your name: ");
        String name = in.next();
        System.out.print("Enter your salary: ");
        double salary = in.nextDouble();

        System.out.print("How many hours you worked this week: ");
        int total_hours_worked = in.nextInt();


        double final_salary = computePay(total_hours_worked,salary);

        System.out.println(final_salary);
        //if hours are greater than 40
        // each hour is paid at 150% of hoursly rate 40 - hours worked = x-hrs
        //x-hrs * (rate * 1.50)
        /*int overtime_hours = 0;
        double overtime_salary =0;
        double compute_salary;

        if(total_hours_worked > 40){
            overtime_hours = total_hours_worked - 40;
            overtime_salary = overtime_hours * (salary * 1.50);
            compute_salary =  overtime_salary + ((total_hours_worked - overtime_hours) * salary);
        }

        else{
         compute_salary = salary * total_hours_worked;
        }


        System.out.println("Paycheck");
        System.out.println("Name: "+name);
        System.out.println("Salary rate: "+salary);
        System.out.println("Hours worked: "+ total_hours_worked);
        if(total_hours_worked >40){
            System.out.println("Overtime hours: "+ overtime_hours);
            System.out.println("Overtime salary: "+ overtime_salary);
        }

        System.out.println("Total salary: £"+ compute_salary);*/
    }

    public static double computePay(int total_hours, double pay_rate){
        double salary = 0;
        int overtime_hrs = 0;
        double overtime_slry;
        if(total_hours > 40){
            overtime_hrs = total_hours - 40;
            overtime_slry = overtime_hrs * (1.50 * pay_rate);
            salary = overtime_slry + (40 * pay_rate);
        }

        else salary = pay_rate * total_hours;

        return salary;
    }
}
