package HashMaps;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Created by azzumwaqar on 25/04/2017.
 */
public class MapDemo {
    public static void main(String [] args){

        //create an array
        String [] english = {"1","2","3","4"};
        String [] miwok = {"Lutti","Otiiko","Tolookosu","Oyyisa"};

        //create a Map: used to form association between keys and vlayes
        Map<Integer,String> miwokNumbers = new HashMap<Integer, String>();

        //add associations
        miwokNumbers.put(1,"Lutti");
        miwokNumbers.put(2,"otiiko");
        miwokNumbers.put(3,"tolookosu");
        miwokNumbers.put(4,"oyyisa");
        miwokNumbers.put(5,"massokka");
        miwokNumbers.put(6,"temmokka");
        miwokNumbers.put(7,"kenekaku");
        miwokNumbers.put(8,"kawinta");
        miwokNumbers.put(9,"wo'e");
        miwokNumbers.put(10,"na'aacha");


        //iterate and print
        Set<Integer> keySet = miwokNumbers.keySet();
        for(int i:keySet){
            System.out.println(i + "=>"+miwokNumbers.get(i));
        }

        System.out.println();
        System.out.println("Number "+ 7 + " is called "+ getMiwokNumber(miwokNumbers,7) + " in Miwok.");

        System.out.println("-------");

       Map<String,String> map = fillMap(english,miwok);

       Set<String> key_Set = map.keySet();
       for(String key: key_Set){

           String value = map.get(key);
           System.out.println(key + " -> " + value);
       }

    }

    public static String getMiwokNumber(Map<Integer,String> map, int i){
        String word = map.get(i);
        return word;
    }

    public  static Map<String,String> fillMap(String [] array,String [] array2){

        Map<String,String> map = new HashMap<String, String>();

        for(int i=0; i < array.length; i++){
            map.put(array[i], array2[i]);
        }
        return map;
    }



}
