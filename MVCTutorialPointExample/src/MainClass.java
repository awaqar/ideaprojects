/**
 * Created by azzumwaqar on 27/08/2017.
 */
public class MainClass {

    public static void main(String [] args){
        Student_Model student= retreiveStudentFromModel();

        Student_View student_view = new Student_View();

        Student_Controller student_controller = new Student_Controller(student,student_view);

        student_controller.updateView();

        student_controller.setStudentName("Emre");

        student_controller.updateView();

        student_controller.setStudentRoll(345);

        student_controller.updateView();

    }



    private static Student_Model retreiveStudentFromModel(){
        Student_Model student_model = new Student_Model();
        student_model.setName("Azzum");
        student_model.setRoll_no(123);
        return  student_model;

    }
}

