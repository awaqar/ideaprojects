/**
 * Created by azzumwaqar on 27/08/2017.
 */
public class Student_Controller {
    Student_Model model;
    Student_View view;

    public Student_Controller(Student_Model model,Student_View view){
        this.view = view;
        this.model = model;
    }

    public String getStudentName() {
        return model.getName();
    }

    public void setStudentName(String name) {
        this.model.setName(name);
    }

    public int getStudentRoll() {
        return model.getRoll_no();
    }

    public void setStudentRoll(int roll) {
        this.model.setRoll_no(roll);
    }

    public void updateView(){
        view.printStudentDetails(getStudentName(),getStudentRoll());
    }
}
